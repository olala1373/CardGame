import javax.swing.ImageIcon;


public class Icon {
	private ImageIcon icon[]=new ImageIcon[8];
	public void icon(){
		icon[0]=new ImageIcon("Argentina.png");
		icon[1]=new ImageIcon("Brazil.png");
		icon[2]=new ImageIcon("England.png");
		icon[3]=new ImageIcon("France.png");
		icon[4]=new ImageIcon("Germany.png");
		icon[5]=new ImageIcon("Iran.png");
		icon[6]=new ImageIcon("Netherlands.png");
		icon[7]=new ImageIcon("Portugal.png");
	}
	public void icon2(){
		icon[0]=new ImageIcon("art_deco_clock_black.png");
		icon[1]=new ImageIcon("art_deco_clock_blue.png");
		icon[2]=new ImageIcon("art_deco_clock_cream.png");
		icon[3]=new ImageIcon("art_deco_clock_green.png");
		icon[4]=new ImageIcon("art_deco_clock_orange.png");
		icon[5]=new ImageIcon("art_deco_clock_violet.png");
		icon[6]=new ImageIcon("art_deco_clock_white.png");
		icon[7]=new ImageIcon("aXanaclock-blu.png");
	}
	public void icon3(){
		icon[0]=new ImageIcon("ie4.png");
		icon[1]=new ImageIcon("ie5.png");
		icon[2]=new ImageIcon("ie9.png");
		icon[3]=new ImageIcon("ie10.png");
		icon[4]=new ImageIcon("ie11.png");
		icon[5]=new ImageIcon("ie12.png");
		icon[6]=new ImageIcon("ie18.png");
		icon[7]=new ImageIcon("ie19.png");
	}
	public ImageIcon geticon(int i){
		return icon[i];
	}
	
}
