import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;


public class Score {
	
	private int score=0;
	private boolean f=false;
	private int i = 0;
	private int highscore=0;

	public void calcscore(){
		
		score+=100;
		if(getbonus()){
			score+=((i+1)*50);
			i++;
		}
		else i=0;
		setscore(score);
	}
public void setscore(int score){
	this.score=score;
}
public int getscore(){
	return score;
}
public boolean getbonus(){
	return f;
}
public void setbonus(boolean f){
	this.f=f;
}
public void calchighscore() throws IOException{
	if(highscore<getscore()){
		highscore=getscore();
		sethighscore(highscore);
		ObjectOutputStream fail = new ObjectOutputStream(new FileOutputStream("file.txt"));
		fail.writeObject(highscore);
		fail.close();
	}
	
}
public void sethighscore(int highscore){
	this.highscore=highscore;
}
public int gethighscore(){
	return highscore;
}
}
