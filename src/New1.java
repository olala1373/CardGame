import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class New1 extends JFrame implements MouseListener {
	
    private JFrame fr=new JFrame("My memory game ;P");
    
    private JButton[][] bt=new JButton[4][4];
    private JButton[] scbt=new JButton[4];
    private JButton[] fobt=new JButton[5];
    private ImageIcon icon[]=new ImageIcon[8];
    private  int check=0;
    private Board br=new Board();
    private int I=0,J=0,dI,dJ;
    private int x=0,dx=0;
	private int y=0,dy=0;
	private boolean flag=false,flag2=false,flag3=false;
	private int temp=0;
    private int temp2=0;
    private Score sc=new Score();
    private int foul=0,goal=0,win=0;
    private JMenuBar menu = new JMenuBar();
    private JMenuItem help;
    private JMenu file;
	private JMenu info;
	private JMenuItem newfile;
	private JMenuItem stage1;
	private JMenuItem stage2;
	private JMenuItem stage3;
	private JMenuItem quit;
	private JMenuItem about;
	private Icon ic=new Icon();
	private int highscore=0;
    
    public New1(){
    	Frame();
    	menu();
    	JOptionPane.showMessageDialog(fr, "please choose a stage to start...");
    	stage1.addMouseListener(this);
    	stage2.addMouseListener(this);
    	stage3.addMouseListener(this);
    	//scorebutton();
    	//click();
    }
	public void Frame(){
		
		fr.setSize(1025,1000);
		fr.setDefaultCloseOperation(EXIT_ON_CLOSE);
		fr.setVisible(true);	
		fr.setLayout(null);
		fr.setResizable(false);
		
	}
	////////////////////////////////////
	public void menu(){
		   file=new JMenu("file");
			info = new JMenu("about");
			quit=new JMenuItem("quit");
			newfile=new JMenuItem("new");
			stage1=new JMenuItem("stage 1");
			stage2=new JMenuItem("stage 2");
			stage3=new JMenuItem("stage 3");
			help=new JMenuItem("help");
			about=new JMenuItem("about");
			menu.add(file);
			menu.add(info);
			file.add(stage1);
			file.add(stage2);
			file.add(stage3);
			file.add(quit);
			info.add(help);
			info.add(about);
			fr.setJMenuBar(menu);
			//newfile.addMouseListener(this);
	}
	//////////////////////////////////////
    
	//////////////////////////////////////////

   public void Button(){
	   try {
			fr.setContentPane(new JLabel(new ImageIcon(ImageIO.read(new File("1.jpg")))));
		} catch (IOException e) {
			e.printStackTrace();
		}
	   for(int i=0;i<4;i++){
			for(int j=0;j<4;j++){	
			bt[i][j]=new JButton(" ");
			bt[i][j].setIcon(null);
			bt[i][j].setSize(128,128);
			bt[i][j].setLocation(100+(j*128), 100+(i*128));
			bt[i][j].setBackground(Color.decode("#FFFFCC"));
			fr.add(bt[i][j]);
			
		}
	}
	   br.board();
	   ic.icon();
   }
  public void Button2(){
	  
	  try {
			fr.setContentPane(new JLabel(new ImageIcon(ImageIO.read(new File("2.jpg")))));
		} catch (IOException e) {
			e.printStackTrace();
		}
	   
	   for(int i=0;i<4;i++){
			for(int j=0;j<4;j++){	
			bt[i][j]=new JButton(" ");
			bt[i][j].setIcon(null);
			bt[i][j].setSize(128,128);
			bt[i][j].setLocation(100+(j*128), 100+(i*128));
			bt[i][j].setBackground(Color.decode("#FFE3FF"));
			fr.add(bt[i][j]);
			
		}
	}
	   br.board2();
	   ic.icon2();
   }
  
 public void Button3(){
	  
	  try {
			fr.setContentPane(new JLabel(new ImageIcon(ImageIO.read(new File("3.jpg")))));
		} catch (IOException e) {
			e.printStackTrace();
		}
	   
	   for(int i=0;i<4;i++){
			for(int j=0;j<4;j++){	
			bt[i][j]=new JButton(" ");
			bt[i][j].setIcon(null);
			bt[i][j].setSize(128,128);
			bt[i][j].setLocation(100+(j*128), 100+(i*128));
			bt[i][j].setBackground(Color.decode("#DBEADC"));
			fr.add(bt[i][j]);
			
		}
	}
	   br.board3();
	   ic.icon3();
   }
   
   
   /////////////////////////////////////////////////////
   public void scorebutton() throws IOException, ClassNotFoundException{
	   for(int i=0;i<4;i++){
		   scbt[i]=new JButton();
		   scbt[i].setSize(100,100);
		   if(i<2){
		   scbt[i].setLocation(700+(i*100),100);
		   }
		   else scbt[i].setLocation(700+((i-2)*100), 200);
		   fr.add(scbt[i]);
	   }
	   scbt[0].setText("Score");
	   scbt[0].setBackground(Color.getHSBColor(255, 204, 153));
	   scbt[1].setText("lastscore");
	   scbt[1].setBackground(Color.getHSBColor(255, 204, 204));
	   scbt[2].setText("00");
	   scbt[3].setText("00");
	   Color color2 =  Color.decode("#7FFFD4");
	   Color color3 =  Color.decode("#00FFFF");
	   scbt[2].setBackground(color2);
	   scbt[3].setBackground(color3);
	   
	   for(int i=0;i<5;i++){
		   fobt[i]=new JButton();
		   fobt[i].setSize(50, 50);
		   fobt[i].setLocation(280+(i*50), 650);
		   fr.add(fobt[i]);
		   fobt[i].setBackground(Color.white);
	   }
	   FileInputStream file = new FileInputStream("file.txt");
		ObjectInputStream file2 = new ObjectInputStream(file);
		 highscore = (int)file2.readObject();
		 
		scbt[3].setText(""+highscore);
	   
   }
//////////////////////////////////////////////////////
   public void click(){
	   try{
		    for(int i=0;i<4;i++){
		   for(int j=0;j<4;j++){
			   bt[i][j].addMouseListener(this);
		   }
	   }
	   }
	   catch(NullPointerException e){
		   JOptionPane.showMessageDialog(fr, "please enter on a button...");
	   }
	   catch(ArrayIndexOutOfBoundsException e){
		   JOptionPane.showMessageDialog(fr, "please enter on a button");
	   }
        quit.addMouseListener(this);
        about.addMouseListener(this);
        help.addMouseListener(this);
	  
   }
//////////////////////////////////////////////////////
													
public void mouseClicked(MouseEvent e) {		
	// TODO Auto-generated method stub					
														
}

@Override
public void mouseEntered(MouseEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void mouseExited(MouseEvent e) {
	// TODO Auto-generated method stub
	
}

//////////////////////////////////////////////////
public void mousePressed(MouseEvent e) {
	
	if(quit==e.getSource()){
		
	    int i=JOptionPane.showConfirmDialog(fr, "are you sure?");
	    if(i==0){
		System.exit(0);
	    }
	}
	if(stage1==e.getSource()){
		//JOptionPane.showConfirmDialog(fr, "are you sure?");
		Button();
		try {
			scorebutton();
		} catch (ClassNotFoundException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		foul=0;
		win=0;
		click();
		sc.setscore(0);
		
	}
	if(stage2==e.getSource()){
		Button2();
		try {
			scorebutton();
		} catch (ClassNotFoundException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		click();
		foul=0;
		win=0;
		sc.setscore(0);
	}
	if(stage3==e.getSource()){
		Button3();
		try {
			scorebutton();
		} catch (ClassNotFoundException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		click();
		foul=0;
		win=0;
		sc.setscore(0);
	}
	if(help==e.getSource()){
		JOptionPane.showMessageDialog(fr,"this is the memory game you should memorize icons that are similar and remove them too win");
	}
	if(about==e.getSource()){
		JOptionPane.showMessageDialog(fr, "designed by MohammadReza Baqalpur(MRB)\n Version:1.1\n random board number included","about", JOptionPane.INFORMATION_MESSAGE);
	}
	
	///////////////////////////////////////
	    y=e.getPoint().y;
	    dy=e.getYOnScreen();
	    x=e.getPoint().x;
	    dx=e.getXOnScreen();
	    int i=dy-y;
	    int j=dx-x;
	    System.out.println(i);
	    System.out.println("..."+j);
	    int help=br.getboard(br.getcellx(i), br.getcelly(j));
	    for(int k=0;k<4;k++){
	    	for(int m=0;m<4;m++){
	    		if(bt[k][m]==e.getSource()){
	    			
	    			bt[k][m].setIcon(ic.geticon(help-1));
	    		    check++;
	    		if(check==3){
	    			if(temp==temp2&&((I!=dI)||(J!=dJ))){
	    				win++;
	    				goal++;
	    				sc.setbonus(flag2);
	    				sc.calcscore();
	    				try {
							sc.calchighscore();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
	    				bt[I][J].setVisible(false);
	    				bt[dI][dJ].setVisible(false);
	    				
	    				scbt[2].setText(""+sc.getscore());
	    				flag2=true;
	    			}
	    			else {
	    				bt[I][J].setIcon(null);
	    				bt[dI][dJ].setIcon(null);
	    				flag2=false;
	    				try{
	    					if(goal==2&&foul>0){
	    						foul--;
	    						fobt[foul].setBackground(Color.white);
	    					}
	    					else{ fobt[foul].setBackground(Color.red);
	    				          foul++;
	    					}
	    				}
	    				catch(ArrayIndexOutOfBoundsException ex){
	    					JOptionPane.showMessageDialog(fr, "GAME OVER");
	    					int q=JOptionPane.showConfirmDialog(fr, "try again?");
	    					if(q==0){
	    						JOptionPane.showMessageDialog(fr,"choose a stage");
	    					}
	    					if(q==1||q==2){
	    						System.exit(0);	    					
	    					}
	    				}
	    				goal=0;
	    			}
	    			check=1;
	    		}
	    
	    	 if(check==1){
	    	I=br.getcellx(i);
	    	J=br.getcelly(j);
	        temp=help;
	    }
	    	 if(check==2){
	    	dI=br.getcellx(i);
	    	dJ=br.getcelly(j);
	        temp2=help;
	    }
	    }
	    
       } 
      }
	    if(win==8){
	    	JOptionPane.showMessageDialog(fr, "Win");
	    }
	//////////////////////////////////////////////
}

////////////////////////////////////////////////////////
public void mouseReleased(MouseEvent e) {
	// TODO Auto-generated method stub
	
}




}
